import argparse
import logging
import sys
from flask import Flask, request


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
app = Flask(__name__)


@app.route("/notification", methods=["POST"])
def notification_received():
    logging.info("Notification received. Data: {}".format(request.get_json()))
    return "OK" 


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Server for the VIoTD evaluation.")
    parser.add_argument("--port", dest="port", required=True, type=int, help="The port the sensor should listen on.")
    args = parser.parse_args()
    
    app.run(host = "0.0.0.0", port = args.port)
